function slicer (text) {
  let date = text.slice(0, 11)
  return date;
}
class Element {
  constructor (elId,html) {
    this.elId = elId;
    this.html = html;
  }
  render() {
    const element = document.getElementById(this.elId);
          if (element) {
            element.innerHTML = this.html;
          }
  }
}

class WeatherWidget {
  constructor(api) {
    this.api = api;
  }
  getWeather(coords){
    let param = 'lat=' + parseFloat((coords.latitude).toFixed(6)) + '&lon=' +          parseFloat((coords.longitude).toFixed(6));
    let key = "6b65b2eeab376aff39c08df8023f48fc";
    fetch(
      "https://api.openweathermap.org/data/2.5/weather?lang=ru&" + param + "&appid=" +
        key
    )
      .then(function (resp) {
        return resp.json();
      }) 
      .then(function (data) {
      console.log(data)
        drawWeather(data);
      })
      .catch(function () {
      
      });
  }
  }



class WeatherWidgetThree extends WeatherWidget{
  constructor(api) {
    super(api)
  }
  async getWeatherThree(coords){
    let param = 'lat=' + parseFloat((coords.latitude).toFixed(6)) + '&lon=' +          parseFloat((coords.longitude).toFixed(6));
    let key = "6b65b2eeab376aff39c08df8023f48fc";
     let response = await fetch("https://api.openweathermap.org/data/2.5/forecast?lang=ru&" + param + "&appid="+ key)
     if (response) {
       let jsone = await response.json();
       console.log(jsone)
       if (jsone) {
         drawWeatherThree(jsone);
       }
     }
}
}



  function drawWeather(d) {
    
    let celcius = Math.round(parseFloat(d.main.temp) - 273.15);
    document.getElementById("icon").innerHTML = `<img src="https://openweathermap.org/img/wn/${d.weather[0]['icon']}@2x.png">`;
    document.getElementById("description").innerHTML = d.weather[0].description;
    document.getElementById("temp").innerHTML = celcius + "&deg;C";
    document.getElementById("location").innerHTML = `сейчас в ${d.name}`;
    document.getElementById("wind").innerHTML = `ветер ${d.wind.speed} м/c`;
  }
  
  function drawWeatherThree(d) {
    let time = d.list[0].dt_txt;
    let hours = 8-(Number(time.slice(11, 13)))/3 + 4;
    let dateOne = slicer(d.list[hours].dt_txt);
    let dateTwo = slicer(d.list[hours+8].dt_txt);
    let dateThree = slicer(d.list[hours+16].dt_txt);
    console.log("часы"+hours);
   let celcius = Math.round(parseFloat(d.list[hours].main.temp) - 273.15);
    console.log("drawWeatherThree");
    document.getElementById("iconone").innerHTML = `<img src="https://openweathermap.org/img/wn/${d.list[hours].weather[0].icon}@2x.png">`;
    document.getElementById("descriptionone").innerHTML = d.list[hours].weather[0].description;
   document.getElementById("tempone").innerHTML = celcius + "&deg;C";
   document.getElementById("locationone").innerHTML = `${dateOne} в ${d.city.name}`;
    console.log(d.list[hours].wind.speed);
    document.getElementById("windone").innerHTML = `ветер ${d.list[hours].wind.speed} м/c`;
    
    let celciustwo = Math.round(parseFloat(d.list[hours+8].main.temp) - 273.15);
    document.getElementById("icontwo").innerHTML = `<img src="https://openweathermap.org/img/wn/${d.list[hours+8].weather[0].icon}@2x.png">`;
    document.getElementById("descriptiontwo").innerHTML = d.list[hours+8].weather[0].description;
   document.getElementById("temptwo").innerHTML = celciustwo + "&deg;C";
   document.getElementById("locationtwo").innerHTML = `${dateTwo} в ${d.city.name}`;
    document.getElementById("windtwo").innerHTML = `ветер ${d.list[hours+8].wind.speed} м/c`;
    
    let celciusthree = Math.round(parseFloat(d.list[hours+16].main.temp) - 273.15);
    document.getElementById("iconthree").innerHTML = `<img src="https://openweathermap.org/img/wn/${d.list[hours+16].weather[0].icon}@2x.png">`;
    document.getElementById("descriptionthree").innerHTML = d.list[hours+16].weather[0].description;
   document.getElementById("tempthree").innerHTML = celciusthree + "&deg;C";
   document.getElementById("locationthree").innerHTML = `${dateThree} в ${d.city.name}`;
    document.getElementById("windthree").innerHTML = `ветер ${d.list[hours+16].wind.speed} м/c`;
  }
  window.onload = function () {
    let weath = new Element("weather",`<div id = "first"><p id="close"></p>
    <button id="button">прогноз на три дня</button>
    <div id="icon"></div>
    <div id="location"></div>
    <h1 id="temp"></h1>
    <div id="description"></div>
    <div id="wind"></div></div>`)
    let forecast = document.getElementById("forecast");
    forecast.onclick = function () {
      weath.render();
      let weat = new WeatherWidget();
      navigator.geolocation.getCurrentPosition(({coords}) => {
          weat.getWeather(coords);
        })
    
    let button = document.getElementById("button");
    button.onclick = function () {
      let weathOne = new Element("weatherblock",` <div id="block">
    <p id="closeone"></p>
    <div id="weatherone">
     <div id="iconone"></div>
     <div id="locationone"></div>
     <h1 id="tempone"></h1>
     <div id="descriptionone"></div>
     <div id="windone"></div>
   </div>
    <div id="weathertwo">
     <div id="icontwo"></div>
     <div id="locationtwo"></div>
     <h1 id="temptwo"></h1>
     <div id="descriptiontwo"></div>
     <div id="windtwo"></div>
   </div>
   <div id="weatherthree">
     <div id="iconthree"></div>
     <div id="locationthree"></div>
     <h1 id="tempthree"></h1>
     <div id="descriptionthree"></div>
     <div id="windthree"></div>
     </div>
     </div>`)
     weathOne.render();
     let closeone = document.getElementById("closeone");
    let weatherBlock = document.getElementById("block")
    closeone.onclick = function () {
      weatherBlock.remove(weatherBlock);
    }
    let weathThree = new WeatherWidgetThree() 

    navigator.geolocation.getCurrentPosition(({coords}) => {
        weathThree.getWeatherThree(coords);
      })}
    
    let close = document.getElementById("close");
    let weather = document.getElementById("first")
    close.onclick = function () {
      weather.remove(weather);
    }
      }

    
  };
  
  